﻿using UnityEngine;
using UnityEngine.UI;

public class SabotageMonsterManager : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private Text
        PersonalStats,
        TypeTitle, TypeValue, SubTypeTitle, SubTypeValue,
        SawTitle, SawValue, CatchTitle, CatchValue,
        ToEvolveTitle, ToEvolveValue,
        CombatStats,
        SabotageTitle,
        SabBaseTitle, SabBaseValue, SabLevelTitle, SabLevelValue,
        AttackTitle,
        AtkBaseTitle, AtkBaseValue, AtkLevelTitle, AtkLevelValue,
        DefenseTitle,
        DefBase, DefBaseValue, DefLevelTitle, DefLevelValue,
        HealthTitle,
        HpBaseTitle, HpBaseValue, HpLevelTitle, HpLevelValue;
    [SerializeField]
    private Image MonsterHead;
    [SerializeField]
    private GameObject MonsterShield, Stats;
#pragma warning restore 0649
    public void Initialize(Monster monster)
    {
        MonsterShield.SetActive(true);
        Stats.SetActive(true);
        MonsterPowers powers = monster.GetPowers();        
        ImagesFillers.AddMonsterHead(MonsterHead, monster.MonsterName);
        LanguagesFillers.FillMonsterPersonalStats(PersonalStats, TypeTitle, SubTypeTitle, SawTitle, CatchTitle, ToEvolveTitle);
        LanguagesFillers.FillSabMonsterCombatStats(CombatStats,
            SabotageTitle, SabBaseTitle, SabLevelTitle,
            AttackTitle, AtkBaseTitle, AtkLevelTitle,
            DefenseTitle, DefBase, DefLevelTitle,
            HealthTitle, HpBaseTitle, HpLevelTitle);
        TypeValue.text = monster.Type.ToString();
        SubTypeValue.text = monster.SubType.ToString();
        SawValue.text = monster.Stats.SeenAmount.ToString();
        CatchValue.text = monster.Stats.CatchedAmount.ToString();
        ToEvolveValue.text = monster.Stats.CatchesToEvolve + " " + LanguagesFillers.Lang.ToCatch;
        SabBaseValue.text = MathConts.RoundNumber(powers.SabotagePower.Base,3).ToString();
        SabLevelValue.text = MathConts.RoundNumber(powers.SabotagePower.LevelSabotage,3).ToString();
        if (powers.AttackPower.Base == 0)
        {
            AtkBaseValue.text = MathConts.RoundNumber((powers.SabotagePower.Total / 2),3).ToString();
            AtkLevelValue.text = "0";
            DefBaseValue.text = MathConts.RoundNumber(powers.DefensePower.Base,3).ToString();
            DefLevelValue.text = MathConts.RoundNumber(powers.DefensePower.LevelDefense,3).ToString();
        }
        else
        {
            AtkBaseValue.text = MathConts.RoundNumber(powers.AttackPower.Base,3).ToString();
            AtkLevelValue.text = MathConts.RoundNumber(powers.AttackPower.LevelAttack,3).ToString();
            DefBaseValue.text = MathConts.RoundNumber((powers.SabotagePower.Total / 2),3).ToString();
            DefLevelValue.text = "0";
        }
        HpBaseValue.text = MathConts.RoundNumber(powers.HealthPower.BaseHp,3).ToString();
        HpLevelValue.text = MathConts.RoundNumber(powers.HealthPower.LevelHp,3).ToString();
    }
}
