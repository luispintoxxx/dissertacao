﻿using UnityEngine;
using UnityEngine.UI;

public class AttackDefenseMonsterManager : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private Text
        PersonalStats,
        TypeTitle, TypeValue, SubTypeTitle, SubTypeValue,
        SawTitle, SawValue, CatchTitle, CatchValue,
        ToEvolveTitle, ToEvolveValue,
        CombatStats,
        AttackTitle,
        AtkBaseTitle, AtkBaseValue, AtkLevelTitle, AtkLevelValue,
        DefenseTitle,
        DefBase, DefBaseValue, DefLevelTitle, DefLevelValue,
        HealthTitle,
        HpBaseTitle, HpBaseValue, HpLevelTitle, HpLevelValue;
    [SerializeField]
    private Image atkHead, defHead;
    [SerializeField]
    private GameObject AtkShield, DefShield, Stats;
#pragma warning restore 0649
    public void Initialize(Monster monster)
    {
        if (monster.Type == MonsterType.ATTACK)
        {
            AtkShield.SetActive(true);
            DefShield.SetActive(false);
            ImagesFillers.AddMonsterHead(atkHead, monster.MonsterName);
        }
        else
        {
            AtkShield.SetActive(false);
            DefShield.SetActive(true);
            ImagesFillers.AddMonsterHead(defHead, monster.MonsterName);
        }
        Stats.SetActive(true);
        MonsterPowers powers = monster.GetPowers();        
        LanguagesFillers.FillMonsterPersonalStats(PersonalStats, TypeTitle, SubTypeTitle, SawTitle, CatchTitle, ToEvolveTitle);
        LanguagesFillers.FillAtkDefMonsterCombatStats(CombatStats, AttackTitle, AtkBaseTitle, AtkLevelTitle, DefenseTitle, DefBase,
            DefLevelTitle, HealthTitle, HpBaseTitle, HpLevelTitle);
        TypeValue.text = monster.Type.ToString();
        SubTypeValue.text = monster.SubType.ToString();
        SawValue.text = monster.Stats.SeenAmount.ToString();
        CatchValue.text = monster.Stats.CatchedAmount.ToString();
        ToEvolveValue.text = monster.Stats.CatchesToEvolve + " " + LanguagesFillers.Lang.ToCatch;
        AtkBaseValue.text = MathConts.RoundNumber(powers.AttackPower.Base,3).ToString();
        AtkLevelValue.text = MathConts.RoundNumber(powers.AttackPower.LevelAttack,3).ToString();
        DefBaseValue.text = MathConts.RoundNumber(powers.DefensePower.Base,3).ToString();
        DefLevelValue.text = MathConts.RoundNumber(powers.DefensePower.LevelDefense,3).ToString();
        HpBaseValue.text = MathConts.RoundNumber(powers.HealthPower.BaseHp,3).ToString();
        HpLevelValue.text = MathConts.RoundNumber(powers.HealthPower.LevelHp,3).ToString();
    }
}
