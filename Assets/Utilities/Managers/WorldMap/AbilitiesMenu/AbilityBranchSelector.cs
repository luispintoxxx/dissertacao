﻿using UnityEngine;
using UnityEngine.UI;

public class AbilityBranchSelector : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private AbilityBranchManager abilityBranchManager;
    [SerializeField]
    private Button LogoBtn;
    [SerializeField]
    private AbilityType type;
    [SerializeField]
    private PanelController controller;
#pragma warning restore 0649
    void Start() => LogoBtn.onClick.AddListener(EnterAbilitiesTypeLine);
    private void EnterAbilitiesTypeLine()
    {
        abilityBranchManager.SelectedAbilityBranch(type);
        controller.GoToNextPanel(controller.abilityMenu);
    }
}
