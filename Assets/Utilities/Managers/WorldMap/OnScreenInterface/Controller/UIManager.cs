﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private Text xptext, leveltext, leveltitle, HelpBtntitle;
    [SerializeField]
    private GameObject menu, levelLoader, tutorialUI;
    [SerializeField]
    private Button helpBtn;
    [SerializeField]
    private Animator transition;
    [SerializeField]
    private AudioSource audioSource;
#pragma warning restore 0649
    private void Awake()
    {
        Initialize();
        helpBtn.onClick.AddListener(() => ToggleTutorial(true));
        SceneTransitionManager.Instance.SetLoader(levelLoader);
        SceneTransitionManager.Instance.SetAnimator(transition);            
    }
    private void OnEnable()
    {
        CheckTutorial();
        Initialize();
        UpdateXP();
    }
    public void UpdateXP() =>
        xptext.text = GameManager.Instance.CurrentPlayer.Xp + " / " + GameManager.Instance.CurrentPlayer.RequiredXp;
    private void ToggleMenu() => menu.SetActive(!menu.activeSelf);
    public void MenuBtnClicked()
    {
        audioSource.Play();
        ToggleMenu();
    }
    public void Initialize()
    {
        HelpBtntitle.text = LanguagesFillers.Lang.Help;
        leveltitle.text = LanguagesFillers.Lang.Level;
        leveltext.text = GameManager.Instance.CurrentPlayer.Level.ToString();
    }
    private void CheckTutorial() => ToggleTutorial(PlayerPrefs.GetInt(SquadUpConstants.WORLDMAP_TUTORIAL) == 0);
    private void ToggleTutorial(bool onOff) => tutorialUI.SetActive(onOff);
}
