﻿using UnityEngine;
using UnityEngine.UI;

public class CaptureUIManager : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private GameObject CaptureUI, LevelLoader, ConfirmationUI, Orb, TutorialUI;
    [SerializeField]
    private Button RunAwayBtn, ContinueBtn, YesBtn, NoBtn, HelpBtn;
    [SerializeField]
    private Text RunAwayTitle, ContinueTitle, ExitQuestion, Yes, No, HelpBtnTitle;
    [SerializeField]
    private Animator transition;
    [SerializeField]
    private MusicPlayer SoundPlayer;
#pragma warning restore 0649
    private void Awake() => HelpBtn.onClick.AddListener(() => ToggleTutorial(true));
    private void Start()
    {
        CheckTutorial();
        HelpBtnTitle.text = LanguagesFillers.Lang.Help;
        LanguagesFillers.FillDuelOptionsButtons(RunAwayTitle, ContinueTitle);
        SceneTransitionManager.Instance.SetLoader(LevelLoader);
        SceneTransitionManager.Instance.SetAnimator(transition);
        RunAwayBtn.onClick.AddListener(() => ActivateConfirmationSubPanel());
        ContinueBtn.onClick.AddListener(() => ContinueToCapture());
    }
    private void CheckTutorial() => ToggleTutorial(PlayerPrefs.GetInt(SquadUpConstants.CAPTURE_TUTORIAL) == 0);
    private void ContinueToCapture()
    {
        ToggleUI(false);
        Orb.SetActive(true);
        OverrideOrb overrideOrb = Orb.GetComponent<OverrideOrb>();
        overrideOrb.SetSoundPlayer(SoundPlayer);
    }
    private void ActivateConfirmationSubPanel()
    {
        ToggleConfirmation(true);
        LanguagesFillers.FillConfirmationSubPanel(ExitQuestion, Yes, No);
        YesBtn.onClick.AddListener(() =>
            {
                GameManager.Instance.CurrentPlayer.RetreatCaptures++;
                SceneTransitionManager.Instance.GoToScene(SquadUpConstants.SCENE_WORLD);
            }
        );
        NoBtn.onClick.AddListener(() => ToggleConfirmation(false));
    }
    private void ToggleTutorial(bool onOff) => TutorialUI.SetActive(onOff);
    private void ToggleUI(bool onOff) => CaptureUI.SetActive(onOff);
    private void ToggleConfirmation(bool onOff) => ConfirmationUI.SetActive(onOff);
}
