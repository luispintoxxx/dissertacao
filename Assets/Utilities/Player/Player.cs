﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Player
{    
    private const int ABILITY_GAINER = 3, ZERO = 0;
    private const float levelBase = 100, levelXpIncrementer = 2.5f, won = 25.0f, lost = 12.5f;
    private readonly string path;
    private string attackMonsterName, defenseMonsterName, sabotageMonsterName = null;
    public float Xp { get; private set; } = ZERO;
    public float RequiredXp { get => levelBase + (Level * levelXpIncrementer); }
    public float NeededXp { get => RequiredXp - Xp; }
    public float LevelBase { get => levelBase; }
    public int Level { get; private set; } = 1;
    public int PlayerPoints { get; set; } = 3; //120
    public int PlayerMaxPoints { get; private set; } = 3; //120
    public int ExtraThrows { get => Level / 10; }
    public int WonBattles { get; private set; } = ZERO;
    public int LostBattles { get; private set; } = ZERO;
    public int RetreatBattles { get; set; } = ZERO;
    public int RetreatCaptures { get; set; } = ZERO;
    public int InspPotions { get; set; } = ZERO;
    public int HpPotions { get; set; } = ZERO;
    public int WinRatio => (TotalBattles > ZERO) ? WonBattles * 100 / TotalBattles : ZERO;
    public int TotalBattles => WonBattles + LostBattles + RetreatBattles;      
    public void PutAttackMonsterName(string name) => attackMonsterName = name;
    public void PutDefenseMonsterName(string name) => defenseMonsterName = name;
    public void PutSabotageMonsterName(string name) => sabotageMonsterName = name;
    public string Username { get; private set; }
    public string Password { get; private set; }
    public string Email { get; private set; }
    public int IncrPoints { get; private set; } = 3;
    public CharsTypes CharType { get; private set; }
    public List<AbilityData> Abilities { get; private set; } = new List<AbilityData>();
    public List<MonsterData> MonsterData { get; private set; } = new List<MonsterData>();
    private Squadron squadron = new Squadron();
    public Squadron Squadron
    {
        get
        {
            if (!squadron.IsReady)
            {
                squadron = new Squadron(
                    GameManager.Instance.MonsterManager.GetMonsterByName(attackMonsterName),
                    GameManager.Instance.MonsterManager.GetMonsterByName(defenseMonsterName),
                    GameManager.Instance.MonsterManager.GetMonsterByName(sabotageMonsterName)
                    );
            }
            return squadron;
        }
    }
    public Player()
    {
        Username = PlayerPrefs.GetString(SquadUpConstants.USERNAME);
        path = Application.persistentDataPath + "/Player-" + Username + ".dat";
        Load();
    }
    public void AddXp(bool win)
    {        
        if (win)
        {
            Xp += won;
            WonBattles++;
        }
        else
        {
            Xp += lost;
            LostBattles++;
        }        
        if (Xp > RequiredXp)
        {
            Xp %= levelBase;
            PlayerPoints += ABILITY_GAINER;
            PlayerMaxPoints += ABILITY_GAINER;
            Level++;
        }
        Save();
    }
    public void StartMonsters()
    {
        MonsterData = GameManager.Instance.MonsterManager.StartMonstersData();
        Save();
    }
    public void UpdateMonsters()
    {
        MonsterData = GameManager.Instance.MonsterManager.GetMonstersData();
        Save();
    }
    public void UpdateAbilities()
    {
        Abilities = GameManager.Instance.AbilitiesContainer.GetAbilitiesData();
        Save();
    }
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(path);
        PlayerData data = new PlayerData(this);
        bf.Serialize(file, data);
        file.Close();
    }
    public void Load()
    {        
        if (File.Exists(path))
        {
            PlayerData Data;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            Data = (PlayerData)bf.Deserialize(file);
            file.Close();
            Username = Data.Username;
            Password = Data.Password;
            Email = Data.Email;
            Xp = Data.Xp;
            Level = Data.Lvl;
            Abilities = Data.Abilities;
            PlayerMaxPoints = Data.PlayerMaxPoints;
            HpPotions = Data.HpPotions;
            InspPotions = Data.InspPotions;
            MonsterData = Data.MonsterData;
            attackMonsterName = Data.AttackMonsterName;
            defenseMonsterName = Data.DefenseMonsterName;
            sabotageMonsterName = Data.SabotageMonsterName;
            WonBattles = Data.WonBattles;
            LostBattles = Data.LostBattles;
            RetreatBattles = Data.RetreatBattles;
            RetreatCaptures = Data.RetreatCaptures;
            CharType = Data.CharType;
        }
        else
        {
            Save();
        }
    }
    public string GetPointsToMax() => PlayerPoints + "/" + PlayerMaxPoints;
}
