﻿using System;
using System.Collections.Generic;

[Serializable]
public class PlayerData
{
    public float Xp { get; private set; }
    public float RequiredXp { get; private set; }
    public float LevelBase { get; private set; }
    public int InspPotions { get; private set; }
    public int HpPotions { get; private set; }
    public int PlayerMaxPoints { get; private set; } 
    public int WonBattles { get; private set; }
    public int LostBattles { get; private set; }
    public int RetreatBattles { get; set; }
    public int RetreatCaptures { get; set; }
    public int Lvl { get; private set; }
    public List<AbilityData> Abilities { get; private set; } = new List<AbilityData>();
    public List<MonsterData> MonsterData { get; private set; } = new List<MonsterData>();
    public string AttackMonsterName { get; set; }
    public string DefenseMonsterName { get; set; }
    public string SabotageMonsterName { get; set; }
    public string Username { get; private set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public CharsTypes CharType { get; private set; }
    public PlayerData(Player player)
    {
        Xp = player.Xp;
        RequiredXp = player.RequiredXp;
        LevelBase = player.LevelBase;
        Lvl = player.Level;
        Abilities = player.Abilities;
        MonsterData = player.MonsterData;
        Username = player.Username;
        Password = player.Password;
        Email = player.Email;
        CharType = player.CharType;        
        PlayerMaxPoints = player.PlayerMaxPoints;
        HpPotions = player.HpPotions;
        InspPotions = player.InspPotions;
        WonBattles = player.WonBattles;
        LostBattles = player.LostBattles;
        RetreatBattles = player.RetreatBattles;
        RetreatCaptures = player.RetreatCaptures;
        AttackMonsterName = player.Squadron.AtkMonster != null ? player.Squadron.AtkMonster.MonsterName : null;
        DefenseMonsterName = player.Squadron.DefMonster != null ? player.Squadron.DefMonster.MonsterName : null;
        SabotageMonsterName = player.Squadron.SabMonster != null ? player.Squadron.SabMonster.MonsterName : null;
    }

    public PlayerData(string username, string password, string email, CharsTypes charsTypes)
    {
        Username = username;
        Password = password;
        Email = email;
        CharType = charsTypes;
        Xp = WonBattles = LostBattles = RetreatBattles = RetreatCaptures = 0;
        RequiredXp = LevelBase = 100;
        Lvl = 1;
        Abilities = new List<AbilityData>();
        AttackMonsterName = DefenseMonsterName = SabotageMonsterName = null;
        PlayerMaxPoints = PlayerMaxPoints > 3 ? PlayerMaxPoints : 3;
    }
}
