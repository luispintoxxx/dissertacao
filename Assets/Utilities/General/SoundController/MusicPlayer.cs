﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField]
    private AudioSource MusicTrackI, MusicTrackII, MusicTrackIII, Success, Throw, Thump;
    private void Awake() => PlayTracks();
    private void PlayTracks()
    {
        switch (MathConts.RandomInt(1, 4))
        {
            case 1: MusicTrackI.Play(); break;
            case 2: MusicTrackII.Play(); break;
            case 3: MusicTrackIII.Play(); break;
        }
    }
    public void PlaySuccessEffect() => Success.Play();
    public void PlayThrowEffect() => Throw.Play();
    public void PlayThumpEffect() => Thump.Play();
}
