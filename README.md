# SquadUp

O SquadUp é um jogo social com o objetivo de aproximar pessoas. Têm como base o Pokémon Go e utiliza outros jogos como base para as suas mecânicas de jogo.

O SquadUp apenas compila para as plataformas Android e desktop e está apenas disponível neste repositório de GitLab.

Atenção:**Este repositório não aceita nenhuma modificação. O repositório contém o código do jogo para fins de arquivo e estudo. Também são incluídos o apk para descarregamento e instalação direta na plataforma Android assim como um package do código para descarregamento e compilação do jogo na plataforma desktop.

Para instalar o apk é disponibilizado o seguinte tutorial:
[TutorialAPK](https://gitlab.com/luispintoxxx/dissertacao/-/blob/99cd1c8648b458950b9e5724727d48923c97ea93/Instala%C3%A7%C3%A3oAPK.pdf)

Para compilar o projeto é disponibilizado o seguinte tutorial:
[TutorialCompilação](https://gitlab.com/luispintoxxx/dissertacao/-/blob/master/Instru%C3%A7%C3%B5es%20Compila%C3%A7%C3%A3o.pdf)

